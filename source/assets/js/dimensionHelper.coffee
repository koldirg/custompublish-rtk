$ = jQuery
$ ->
	$('#page').append('<div class="dimension">')
	dim = $(".dimension")
	dim.css(
		"width": "100px"
		'height': '20px'
		'position': 'fixed'
		'bottom': '0'
		'right': '20px'
		'textAlign': 'center'
		'backgroundColor': 'rgba(255,255,255,.95)'
		'line-height': '20px'
	)

	setDim = ->
		width = $(window).width()
		height = $(window).height()
		dim.text(width + ' x ' + height)

	$(window).resize (event) ->
		setDim()

	setDim()