
# Custompublish Rapid Templating Kit (CP-RTK)

CP-RTK is an extension of [Middleman](http://middlemanapp.com/) that allows for the rapid development of templates based on the [CustomPublish](http://custompublish.com) content management system.

Conditional operators seperate CP's shortcodes from development content, rendering them only when the project is built. This effectively 

To use the app, clone this repo, and at your CLI run `bundle install` to install the needed gems. Next, `bundle exec middleman` will start the local server (port 4567 unless specified otherwise) with the template activated.

Edits made to the /source will update in the browser window.

When you're ready to build, hit `CTRL+C` to exit the server, and `bundle exec middleman build` to create the template.


Helpers:

cp_login : Outputs login form

cp_search : Outputs the basic search form

## Starting from scratch

1.	### Check your Ruby installation
	
	Ruby is the platform that middleman, and by extension [HAML](http://haml.info/), [Compass](http://compass-style.org/), [CoffeeScript](http://coffeescript.org/) and [Slim](http://slim-lang.com/) operate.

	For windows based systems [Windows Ruby Installer](http://rubyinstaller.org/) is the simplest option for ruby installation. Ruby is installed by default on OSX systems, but an update to 1.9.3+ may be required. [Ruby Version Manager](https://rvm.io/) may be employed on both linux and OSX systems to control Ruby versions.

2.	### Install Bundler

	[Bundler](http://bundler.io/) is an application for ruby gem version management. It's a good way to keep a stable environment of gems at hand so that you can easily backtrack through older versions of gems if an upgrade fails.

3.  ### Clone the CP-RTK repo

	Install the repo from your CLI

		git clone https://koldirg@bitbucket.org/koldirg/custompublish-rtk.git

	At this point you can rename the custompublish-rtk folder to the name of your project.

4.	### Adjust config.rb

	The root of the repo contains the config.rb; home to the project settings. The package will function as is, but you may find it useful to adjust the variables at the top of the file - such as changing

		set :cp_stylesheets, "xxxxxx"

	to match your stylesheet ID.

5.	### Run Bundler and test middleman

	To install all the gem's needed for middleman and cp-rtk to function, run the command

		bundle install

	from the project root. This will install all dependencies listed in the Gemfile.

	If the gem install's completes without incident, it's time to test middleman.

		bundle exec middleman

	The above command will execute middleman in server mode, compiling the cp-rtk files in realtime and hosting them from it's built in server, which you can access via

		0.0.0.0:4567

	You can hit `CTRL+C` in the CLI at any time to halt the server.


Now that your environment is ready, you can develop CustomPublish templates locally. Changes you make to Sass, CSS, JS, coffeescript, slim, haml and html files will be recompiled instantly while the server is running. 

Any mistakes made in the code will be reported in the brwoser window and the CLI.

# Building the template

When you#re happy with your edits and you're ready to build the project, you can and `bundle exec middleman build` to create the template.

Conditional operators are used throughout the default template to seperate CustomPublish shortcodes from development content, rendering them only when the project is built. This effectively 
Helpers:

cp_login : Outputs login form

cp_search : Outputs the basic search form

