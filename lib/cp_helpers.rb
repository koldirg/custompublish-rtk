####
#
#   This file contains a list of methods (helpers) to emulate
#   CustomPublish CMS functionality within middleman. Invoke
#   helpers like you would a keyword, for example:
#
#   =cp_login
#
#   When invoked inside a template, will output the standard
#   login form HTML which you can then style.
#
####

# CP_LOGIN : Produces Login form

def cp_login()
	'<div id="loginbox">
		<form action="/index.php?cat=292004" id="cploginform" method="post" name="cploginformname" style="display:inline">
			<input name="gologin" type="hidden" value="1">
			<input name="cat" type="hidden" value="292004"> 
			<input name="mypage" type="hidden" value="">
				<em style="color:#c00000"><span id="loginerror"></span></em>
			<table border="0" id="logintable" width="350">
				<tbody>
					<tr>
						<td class="default" colspan="2" id="loginheader">
							<h1 class="loginheader">Log in</h1>
						</td>
					</tr>

					<tr>
						<td class="default" id="loginemaillabel">E-mail:</td>
						<td class="default"><input class="logininput" id="loginuserid" name="user" size="24" type="text" value=""></td>
					</tr>

					<tr>
						<td class="default" id="loginpasswordlabel">Password:</td>
						<td class="default"><input class="logininput" id="loginuserpassid" name="pass" size="24" type="password" value=""></td>
					</tr>

					<tr>
						<td class="default" colspan="2">
							<span class="logintext">
								<input class="checkbox" name="rememberme" type="checkbox" value="1"> Remember me next time
							</span>
						</td>
					</tr>

					<tr>
						<td class="default"><input class="loginbutton" name="login" type="submit" value="Log in"></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>'
end

# CP_SEARCH
#   Produces a basic search form
#   Leave blank for input type submit
#   or :type => "button" for a button
#   based submit

def cp_search(opt = {})

	opt[:submit] ||= "input"

	if opt[:submit] == "button" then
		submitType = '<button class="searchsubmit" value="S&oslash;k" type="submit"><div class="searchicon">S&oslash;k</div></button>'
	else
		submitType = '<input class="searchsubmit" value="S&oslash;k" type="submit">'
	end

	'<form id="searchform" class="searchform" action="index.php" method="get"><input class="searchinput" name="find" value="" type="text">'+submitType+'</form>'
end

# lorempixel
#   Outputs a (random) image of specified dimensions
#   from http://lorempixel.com/.
#   Example: =lorempixel "300x150"
#
#	Use :theme => 'category'
#	to set a specific theme

def lorempixel(dimensions, opt = {})
	
	sizeXY = dimensions.split("x")

	themes = [ 'abstract','animals','business','cats','city','food','nightlife','fashion','people','nature','sports','technics','transport' ]

	opt[:theme] ||= themes.sample

	'<img src="http://lorempixel.com/'+sizeXY[0]+'/'+sizeXY[1]+'/'+opt[:theme]+'">'
end