#############################
#	Project variables
#############################

set :site_title, "site"
set :cp_stylesheets, "xxxxxx"
set :cp_javascripts, "xxxxxx"

#############################
# Asset locations
#############################

set :css_dir, 'assets/css'
set :js_dir, 'assets/js'
set :images_dir, 'assets/images'
set :fonts_dir, 'assets/fonts'
set :relative_links, false
set :partials_dir, '/partials'

#############################
#	Slim
#############################

set :slim, :layout_engine => :slim
Slim::Engine.set_default_options :pretty => true, :sort_attrs => false
Slim::Engine.disable_option_validator!

#############################
#	Helpers
#############################

helpers do
	require 'lib/cp_helpers'
end

##########################
#	Compass
##########################

#	Compass configuration

compass_config do |config|
	config.output_style = :expanded
	config.sass_options = {
		:line_comments => false,
		:debug_info => false
	}
end

#	Susy grids in Compass
#	http://susy.oddbird.net/

require 'susy'

#############################
#	Layout templates
#############################

#page "/admin/*", :layout => "admin"
#page "/login.html", :layout => "admin"

#############################
# Livereload
#############################

activate :livereload, :host => '127.0.0.1'

#############################
#	Build configuration
#############################

configure :build do

	set :build, true

	compass_config do |config|
		config.output_style = :expanded
		config.sass_options = {
			:line_comments => false,
			:debug_info => false
		}
	end

	# activate :minify_css
	# activate :minify_javascript
	# activate :cache_buster
	# activate :relative_assets
	# activate :smusher
	# set :http_path, "/Content/images/"
end